# Guide for reproduce results #

This repository describe results MorphoBabushka team in the Dialogue-2020 shared task on taxonomy enrichment for the russian language.

### Approaches ###

* Hypernyms-of-Co-Hyponyms (HCH)
* Definition Processing (DP)
* Combined HCH + DP

### HCH ###
Run *reproduce_best_w2v.sh* for reproduce the best result based on HCH.

* You need at least 14Gb RAM for compute word2vec nearest neighbours and [download](https://zenodo.org/record/400631#.XuFZDpZRUW1) vectors of words to the *w2v* directory. 
* If you don't have the required amount of RAM, comment 10-12 lines in the script. There are computed word2vec nearest neighbours for public/private test set in the *w2v/res/*.

Run *reproduce_w2v.sh* for experimenting with other hyperparameters.

Arguments:

* *path_words* - related path to file with words, file name with nouns must start with 'noun', with verbs - 'verb'
* *topk* - amount of nearest neighbours for target word
* *dist* - measure for similarity between vectors (*sim* or *simcosmul*)
* *is_posmap* - use POS mapping or not
* *levels* - tuple of hypernyms ordering and merging them

### DP ###

Run *run.sh* from directory *quotesbot* to get wiktionary dataset (scrapy library required). It will require time, may be days or more to get the full dataset. One can use processed results from directory *quotesbot*: all files *.tsv.

We provided our results of some different number of iterations (see *quotesbot/meanings_i.tsv*.

The same files was copied to *adiitional_resources* directory. Required for DP method number of iteration parameter was supposed to be 30. Corresponded file *meaning30.csv* was renamed to *meaning.csv*.

Run *reproduce_dp.sh* to reproduce result of DP method. Required networkx library.
Only hyperparameter is number of iterations (see *quotesbot/script.py*).

### Combined HCH + DP ###

One can reproduce combined results from python console by running reproduce_combiner.py.
It requires files: results of HCH and DP methods.
Every file name of directory may be changed by changing first rows in script.
By default, output file is *result.tsv*.
