#!/usr/bin/env python3
from typing import List, Tuple
from itertools import cycle
from networkx.exception import NetworkXError
from json import load


class NetworkXTaxonomy(object):
    def __init__(self, taxonomy_path, *, table_txt2id, table_id2txt):
        from networkx.readwrite.gpickle import read_gpickle
        self.graph = read_gpickle(taxonomy_path)
        with open(table_txt2id, 'r') as fp:
            self.table_txt2id = load(fp)
        with open(table_id2txt, 'r') as fp:
            self.table_id2txt = load(fp)

    def eval(self, query, *, lim=10, order_par=2) -> List[List[str]]:
        question = self.translate_txt2id(query)
        tmp = [tuple(self.get_answer(q, lim, order_par)) for q in question]
        result = []
        for order in range(order_par):
            for q in tmp:
                hypers1 = q[order]
                for h in hypers1:
                    if not(h in result):
                        result.append(h)
        result.sort(key=lambda x: x[1])
        return map(lambda x: str(x[0]), result[:lim])

    def get_answer(self, question, lim, order_par) -> List[str]:
        answer = self.search(question)
        order_par = order_par - 1
        result = answer
        yield result
        while order_par:
            answer, result = result, []
            if len(result) <= lim:
                for hyp, weight in answer:
                    result.extend(self.search(hyp, weight))
                    if len(result) > lim * 3:
                        break
            result.sort(key=lambda x: x[1])
            yield result
            order_par = order_par - 1

    def search(self, question, weight=1.0) -> List[Tuple[List[str], float]]:
        answer = list(self.hyperonyms(question))
        return self.weighted(answer, weight)

    def weighted(self, lst: List[str], w: float) -> List[Tuple[List[str], float]]:
        return list(zip(lst, cycle([1])))

    def add_relation(self, hypo, hyper, **attr):
        self.graph.add_edge(hypo, hyper, **attr)

    def add_relations_from(self, iterator, **attr):
        self.graph.add_edges_from(iterator, **attr)

    def add_new_synset(self, text, **attr):
        if text in self.graph:
            node = str(hash(text)) + text[0]
            while node in self.graph:
                node = str(hash(node)) + node[-1]
        else:
            node = text
        self.graph.add_node(node, **attr)
        self.table_txt2id[text] = [node]
        self.table_id2txt[node] = [text]
        return [node]

    def get_or_create_synset(self, text):
        result = self.table_txt2id.get(text, None)
        if result:
            return result
        else:
            return self.add_new_synset(text)

    def create_synset(self, text):
        if text in self.table_txt2id:
            return []
        else:
            return self.add_new_synset(text)

    def get_synset(self, text):
        return self.table_txt2id.get(text, [])

    def add_node_attr(self, node, **attr):
        self.graph.add_node(node, **attr)

    def add_nodes_attr_from(self, iterator, **attr):
        self.graph.add_nodes_from(iterator, **attr)

    def get_word(self, key):
        if key in self.graph:
            return self.graph[key]
        else:
            self.graph.add_node(key)
            return self.graph[key]

    def get_word_list(self, iterator):
        for node in iterator:
            yield self.get_word(node)

    def enrich(self, word, pid):
        self.table_txt2id[word] = pid
        for synset in pid:
            res = self.table_id2txt.get(synset, [])
            res.append(word)

    def hyponyms(self, key):
        node = self.get_word(key)
        for hypo in self.graph.predecessors(key):
            yield hypo

    def hyperonyms(self, key):
        node = self.get_word(key)
        for hyper in self.graph.successors(key):
            yield hyper

    def hyponyms_from(self, iterator):
        for key in iterator:
            node = self.get_word(key)
            for hypo in self.hyponyms(key):
                yield hypo

    def hyperonyms_from(self, iterator):
        for key in iterator:
            node = self.get_word(key)
            for hyper in self.hyperonyms(key):
                yield hyper

    def translate(self, something):
        result = self.table_id2txt.get(something, None)
        if result is None:
            result = self.table_txt2id.get(something, None)
        return result

    def isin(self, iterable):
        for i in iterable:
            yield i in self.table_txt2id.keys()

    def translate_id2txt(self, synset):
        return self.table_id2txt[synset]

    def translate_txt2id(self, word):
        return self.table_txt2id[word]


class Taxonomy(NetworkXTaxonomy):
    def __init__(self, path, **kwargs):
        super().__init__(path, **kwargs)
