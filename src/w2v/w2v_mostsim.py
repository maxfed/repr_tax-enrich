import gensim
import fire
import sys
from gensim.models import keyedvectors
from collections import defaultdict
from tqdm import tqdm
import pandas as pd
import scipy

def load_w2v(fpath):
    print(f'Loading {fpath}...')
    vv = keyedvectors.Word2VecKeyedVectors.load_word2vec_format(fpath, unicode_errors='ignore', binary=True)
    print(f'Loaded {len(vv.vocab)} words, {vv.vectors.shape} matrix from {fpath}')
    return vv

def print_sim_word(vv, word, fsim, outp, k, vocab):
    if word not in vv.vocab:
        print(word, 0, 0, '', '', sep='\t', file=outp, flush=True)
    else:
        vcount = vocab[word]
        mostsim, scores = zip(*fsim(word, topn=k))
        print(word, vcount, vv.vocab[word].count, ' '.join(mostsim), ' '.join(('%.4f' % s for s in scores)), sep='\t', file=outp, flush=True)

def read_vocab(fpath):
    res = defaultdict(int)
    print(f'Reading vocab file {fpath}...')
    with open(fpath,'rb') as inp:
        v={a.decode('utf-8','replace'):int(b) for a,b in (l.strip().split(b' ') for l in inp.readlines())}
        res.update(v)
    cc = res.values()
    print(f'Vocab file {fpath} is read, containing {len(res)} words with {min(cc), max(cc), sum(cc)/len(cc), sum(cc)} min,max,mean,total counts')
    return res



def print_sim(w2v_fpath, output, topk=300):
    vv = load_w2v(w2v_fpath)
    try:
        vocab = read_vocab(w2v_fpath + '.vocab')
    except FileNotFoundError as e:
        print('WARNING: Vocab file was not found, using will report 0 counts for all words!')
        vocab = defaultdict(int)

    f1, f2 = output + '.sim', output + '.simcosmul'
    with open(f1, 'w') as outp1, open(f2, 'w') as outp2:
        for outp in (outp1, outp2):
            print('word','fvocab_cnt','gensim_cnt','sims','scores',sep='\t',file=outp)
        for s in tqdm(sys.stdin.readlines()):
            s = s.strip().lower()
            print_sim_word(vv, s, vv.most_similar, outp1, topk, vocab)
            print_sim_word(vv, s, vv.most_similar_cosmul, outp2, topk, vocab)
    print('Results saved:', f1, f2, sep='\n')
    df = pd.read_csv(f1,sep='\t')
    for c in ('fvocab_cnt','gensim_cnt'):
        print(df[c].describe(percentiles=[.01,.05,.25,.5,.75,.95,.99]))

fire.Fire(print_sim)
