p=$1
topk=$2
for x in ../../corpora/*w2v ../../w2v/*w2v; do
    echo $p $x
    out=res/$(basename ${p})-$(basename ${x})
    python w2v_mostsim.py $x $out $topk <$p| tee ${out}.log
done
