#!/usr/bin/env python3
from evaluate import BaseClass
from fire import Fire
import pandas as pd
from re import search, compile


class Wiktionary(BaseClass):
    def __init__(self, *,
                 hyper_file='./additional_resources/hyper.tsv',
                 hypo_file='./additional_resources/hypo.tsv',
                 syn_file='./additional_resources/syn.tsv',
                 mean_file='./additional_resources/meaning.tsv',
                 flag_full_syn=False,
                 **kwargs):
        def normalize(df):
            df['PARENT'] = df['PARENT'].apply(str.upper)
            df['PARENT'] = df['PARENT'].apply(lambda x: x.split(','))
            df['WORD'] = df['WORD'].apply(str.upper)
            return df
        super().__init__(**kwargs)
        self.flag_full_syn = flag_full_syn
        args = {'sep': '\t', 'names': ['WORD', 'PARENT'], 'header': None}
        hyper = normalize(pd.read_csv(hyper_file, **args))
        hypo = normalize(pd.read_csv(hyper_file, **args))
        syn = normalize(pd.read_csv(syn_file, **args))
        meaning = pd.read_csv(mean_file,
                              sep='\t')
        meaning['WORD'] = meaning['word'].apply(lambda x: str(x).upper())
        meaning['PARENT'] = meaning['PARENT'].apply(eval)
        meaning = meaning.drop(columns=['word', 'meaning', 'len'])
        self.enrich_taxonomy_syn(
            hyper['WORD'].to_list(),
            hyper['PARENT'].to_list())
        self.enrich_taxonomy_hypo(zip(
            hyper['WORD'].to_list(),
            hyper['PARENT'].to_list()))
        self.enrich_taxonomy_hyper(zip(
            hyper['WORD'].to_list(),
            hyper['PARENT'].to_list()))
        self.enrich_taxonomy_meaning(zip(
            meaning['WORD'].to_list(),
            meaning['PARENT'].to_list()))

    def enrich_taxonomy_meaning(self, lst):
        for hypo, hyper_lst in lst:
            for hyper in hyper_lst:
                if len(hyper) > 4:
                    wid = self.taxonomy.get_or_create_synset(hypo)
                    pid = self.taxonomy.get_synset(hyper)
                    self.taxonomy.add_relations_from(
                        [(i, j) for i in wid for j in pid])

    def enrich_taxonomy_hyper(self, lst):
        for hypo, hyper_lst in lst:
            for hyper in hyper_lst:
                wid = self.taxonomy.get_or_create_synset(hypo)
                pid = self.taxonomy.get_synset(hyper)
                self.taxonomy.add_relations_from(
                    [(i, j) for i in wid for j in pid])

    def enrich_taxonomy_hypo(self, lst):
        for hyper, hypo_lst in lst:
            for hypo in hypo_lst:
                wid = self.taxonomy.get_or_create_synset(hypo)
                pid = self.taxonomy.get_synset(hyper)
                self.taxonomy.add_relations_from(
                    [(i, j) for i in wid for j in pid])

    def enrich_taxonomy_syn(self, word_lst, synonyms):
        for w, word, parent_lst in zip(
                self.taxonomy.isin(word_lst),
                word_lst,
                synonyms):
            for p, parent in zip(self.taxonomy.isin(parent_lst), parent_lst):
                if w and p and self.flag_full_syn:
                    wid = self.taxonomy.translate_txt2id(word)
                    pid = self.taxonomy.translate_txt2id(parent)
                    hyperonyms = self.taxonomy.hyperonyms_from(wid + pid)
                    hyponyms = self.taxonomy.hyponyms_from(wid + pid)
                    self.taxonomy.add_relations_from(zip(pid, hyperonyms))
                    self.taxonomy.add_relations_from(zip(wid, hyperonyms))
                    self.taxonomy.add_relations_from(zip(hyponyms, pid))
                    self.taxonomy.add_relations_from(zip(hyponyms, wid))
                if w != p:
                    if w and not(p):
                        word, parent = parent, word
                    pid = self.taxonomy.translate_txt2id(parent)
                    self.taxonomy.enrich(word, pid)
                if not(w or p):
                    pass

    def fit(self, train_file=None):
        pass

    def decision_function(self, word):
        try:
            result = list(self.taxonomy.eval(word, lim=self.metric_limit))
            if result:
                r = compile(r'[0-9]+\-?\w?')
                result = list(filter(lambda x: bool(search(r, x)), result))
                if len(result) < 10:
                    result = list(self.taxonomy.eval(
                        word, lim=self.metric_limit, order_par=4))
                    r = compile(r'[0-9]+\-?\w?')
                    result = list(filter(lambda x: bool(search(r, x)), result))
                return result
            else:
                return []
        except KeyError:
            return []


if __name__ == '__main__':
    Fire(Wiktionary)
