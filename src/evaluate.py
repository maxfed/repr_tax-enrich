#!/usr/bin/env python3
from collections import defaultdict
from taxonomy import Taxonomy
import sys
from pandas import DataFrame
from fire import Fire
from abc import abstractmethod
prefix = '../'


class BaseClass(object):
    def __init__(self, *,
                 scores_file=prefix+'scores.txt',
                 submit_file=prefix+'submit_file.tsv',
                 truth_file='',
                 metric_limit=10,
                 data_set='train',
                 test_file=prefix + 'data/public_test/nouns_public.tsv',
                 train_file='',
                 **kwargs
                 ):
        """
        scores_file = таблица оценок на dev, по метрикам
        submit_file = файл ответов системы
        truth_file = файл правильных ответов
        metric_limit = предел длины списка ответов, требуется в метриках
        data_file = основной файл таксономии, граф synset_id
        translate_table_txt2id = таблица перевода text to synset_id
        translate_table_id2txt = таблица перевода synset_id to text
        """
        self.metric_limit = metric_limit
        self.test_file = test_file
        self.train_file = train_file
        self.truth_file = truth_file
        self.submit_file = submit_file
        self.scores_file = scores_file
        data_file = prefix + 'data/training_data/' + data_set + '_graph.bin'
        translate_table_txt2id = prefix + \
            'data/translate_tables/' + data_set + '_txt2id.json'
        translate_table_id2txt = prefix + \
            'data/translate_tables/' + data_set + '_id2txt.json'
        self.taxonomy = Taxonomy(data_file,
                                 table_txt2id=translate_table_txt2id,
                                 table_id2txt=translate_table_id2txt)
        print(f'Taxonomy was loaded.')

    @abstractmethod
    def fit(self, *, train_file=None):
        pass

    @abstractmethod
    def decision_function(self, word):
        pass

    def predict(self, *, test_file=None, submit_file=None):
        from codecs import open
        if test_file is None:
            test_file = '../data/public_test/nouns_public.tsv'
        if submit_file is None:
            submit_file = self.submit_file
        with open(test_file, 'r', encoding='utf-8') as input_file:
            test_data = input_file.read().split('\n')[:-1]
        with open(submit_file, 'w', encoding='utf-8') as output_file:
            count_not_presented = 0
            for word in test_data:
                answer = self.decision_function(word)
                if answer:
                    for synset in answer:
                        print(word, synset, self.taxonomy.table_id2txt[synset], sep='\t', file=output_file)
                else:
                    count_not_presented += 1
        print(f'Test {test_file}: Submit file {submit_file} was done.')
        if count_not_presented:
            print(
                f'Warning! {count_not_presented} words not presented in answer file!')

    def score(self, *, submit_file=None, truth_file=None, lim=10):
        if submit_file is None:
            submit_file = self.submit_file
        if truth_file is None:
            truth_file = self.truth_file

        def read_dataset(data_path, sep='\t'):
            from codecs import open
            vocab = defaultdict(list)
            with open(data_path, 'r', encoding='utf-8') as f:
                for line in f:
                    line_split = line.replace('\n', '').split(sep)
                    word = line_split[0]
                    hypernyms = line_split[1]
                    vocab[word].append(hypernyms)
            return vocab
        with open(self.scores_file, 'wt') as out:
            true = read_dataset(truth_file)
            submitted = read_dataset(submit_file)
            if set(true) != set(submitted):
                print('Not all words are presented in your file')
            # --------- compute_score -------------
            MAP, MRR = 0, 0
            out_ex = open(self.scores_file+'_examples', 'w')
            out_ex.write('word :: MAP :: MRR\n')
            for word in true:
                true_dic = {word: true[word]}
                submit_dic = {word: submitted[word]}
                MAP_w, MRR_w = get_score(true_dic, submit_dic, lim)
                MAP, MRR = MAP + MAP_w, MRR + MRR_w
                out_ex.write(word+' :: '+str(MAP)+' :: '+str(MRR)+'\n')
            MAP, MRR = MAP / len(true), MRR / len(true)
            out_ex.close()
            #MAP, MRR = get_score(true, submitted, lim) # general score
            # --------------------------------------
            out.write(f'map: {MAP}\nmrr: {MRR}\n')
            return f'map: {MAP}\tmrr: {MRR}'

    def eval(self):
        self.submit_file = '../dev_answer.tsv'
        self.fit(train_file=self.train_file)
        self.predict(test_file='../data/trial_ask.tsv') #self.predict(test_file='../data/dev_ask.tsv')
        print(self.score(
            submit_file=self.submit_file,
            truth_file='../data/trial_truth.tsv')) #truth_file='../data/truth.tsv'))

    def trial(self):
        self.submit_file = '../trial_submit.tsv'
        self.predict(test_file='../data/trial_ask.tsv')
        print(self.score(
            submit_file=self.submit_file,
            truth_file='../data/trial_truth.tsv'))

    def fire(self):
        self.fit(train_file=self.train_file)
        self.predict(
            test_file='../data/private_test/nouns_private.tsv',
            submit_file='../nouns.tsv')
        self.predict(
            test_file='../data/private_test/verbs_private.tsv',
            submit_file='../verbs.tsv')

    def test(self):
        self.fit(train_file=self.train_file)
        self.predict(
            test_file='../data/public_test/nouns_public.tsv',
            submit_file='../nouns.tsv')
        self.predict(
            test_file='../data/public_test/verbs_public.tsv',
            submit_file='../verbs.tsv')


if __name__ == '__main__':
    Fire(BaseClass)
