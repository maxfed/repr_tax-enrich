from random import choices, seed


def choice(word_list: list, k: int) -> list:
    seed(42)
    return choices(word_list, k=k)
