from os import listdir
from pandas import read_csv, DataFrame, concat
files = listdir('./')
for f in files:
    df = read_csv(f, sep = '\t', compression='gzip')
    key = 'file_name,file_sentences'
    df[key] = df[key].apply(eval)
    df[key] = df[key].apply(lambda lst: ' '.join(lst))
    for i, value in df.iterrows():
        with open('../news.txt', 'a') as fp:
            print(value[key], file = fp)