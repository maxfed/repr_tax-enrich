from collections import defaultdict
from pymorphy2 import MorphAnalyzer
from unicodedata import normalize as uni_norm
from functools import lru_cache
from re import sub as translate
from typing import List, Dict
from Levenshtein import ratio
from re import search
import json
import re
from os.path import exists
morph = MorphAnalyzer()


def add_spaces(word):
    return ' - '.join(word.split('-'))


def main(corpus_list, *, prefix='../../text_corpus/', context = 100):
    with open(f'test_words.tsv', 'r') as file:
        raw = file.read().split()
        raw += [add_spaces(word) for word in raw if '-' in word]

    for corpus in corpus_list:
        if not(exists(f'{prefix}log_{corpus}.txt') or exists(f'{prefix}{corpus}_ps.txt')):
            pattern = create_regular(raw)
            pass_regular(f'{prefix}{corpus}.txt',
                         f'{prefix}log_{corpus}.txt',
                         pattern)
    dictionary = {word: normalize(word) for word in raw}
    whole_set = set()
    for v in dictionary.values():
        whole_set.update(v)
    inverse_dictionary = dict()
    for k, v in dictionary.items():
        for i in v:
            inverse_dictionary[i] = k
    enviroment = inverse_dictionary, dictionary, whole_set
    for corpus in corpus_list:
        if not(exists(f'{prefix}{corpus}_ps.txt')):
            dict_tmp = check_by_morpher(
                [f'{prefix}log_{corpus}.txt'],
                f'{prefix}{corpus}_ps.txt',
                enviroment=enviroment)
        if not(exists(f'{prefix}{corpus}_ps2.txt')):
            correct_by_levenstein(
                [f'{prefix}log_{corpus}.txt'],
                f'{prefix}{corpus}_ps2.txt',
                dict_tmp.keys(),
                suspended_getter(dict_tmp)
            )
        getter_substitutes([f'{prefix}{corpus}_ps.txt', f'{prefix}{corpus}_ps2.txt'],
                           f'{prefix}{corpus}.txt',
                           prefix + 'substitutes/',
                           context)


def create_regular(raw: List[str], thres=3) -> str:
    def secular(word):
        if len(word) >= 6:
            return word[:-len(word)//thres].lower()
        else:
            return word.lower()
    lst = [secular(word) for word in raw]
    lst = lst + [word.capitalize() for word in lst]
    pattern = '[^\w]' + '(' + '|'.join([word.replace('-', '\-')
                                        for word in lst]) + ')' + '[\w]*[^\w]'
    return pattern


def pass_regular(input_file, output_file, pattern):
    import re
    pat = re.compile(pattern)
    with open(input_file, 'r') as file:
        with open(output_file, 'w') as output:
            for i, s in enumerate(file):
                j = re.finditer(pat, s)
                for c in j:
                    sample = c.group(0)[1:-1]
                    print(i, c.start(), sample, c.group(1), file=output)


def normalize(word):
    s = uni_norm('NFKD', word.upper())
    s = translate(r'[^а-яА-Я\-]', '', s)
    return set(normalize_morph(s))


@lru_cache(maxsize=1024*16)
def normalize_morph(word):
    parse = morph.parse(word)
    normal_forms = set([x.normal_form for x in parse])
    if word in normal_forms:
        return set([word])
    else:
        return normal_forms


def check_by_morpher(log_names: List[str], output: str, *, enviroment) -> Dict[str, int]:
    inverse_dictionary, dictionary, whole_set = enviroment
    buffer = None
    counter = {word: 0 for word in dictionary}
    with open(output, 'w') as out:
        for log in log_names:
            with open(log, 'r') as file:
                for line in file:
                    row = line.split()
                    if len(row) < 4:
                        if buffer is None:
                            buffer = line
                            continue
                        else:
                            row = (buffer + line).split()
                            buffer = None
                    sample = normalize(''.join(row[2:-1]))
                    for i in sample:
                        if i in whole_set:
                            word = inverse_dictionary[i]
                            counter[word] += 1
                            print(word.upper(), row[0], row[1], file=out)
    return counter


def suspended_getter(dictionary):
    suspended = list()
    for i, v in dictionary.items():
        if (v < 32):
            suspended.append(i)
    regexpr = create_regular(suspended, 5)
    pattern = re.compile(regexpr)
    return pattern


def correct_by_levenstein(log_names: List[str], output: str, suspended: List[str], pattern) -> Dict[str, int]:
    buffer = None
    counter = {word: 0 for word in suspended}
    with open(output, 'w') as out:
        for log in log_names:
            with open(log, 'r') as file:
                for line in file:
                    row = line.split()
                    if len(row) < 4:
                        if buffer is None:
                            buffer = line
                            continue
                        else:
                            row = (buffer + line).split()
                            buffer = None
                    sample = ''.join(row[2:-1])
                    flag = search(pattern, line)
                    if flag:
                        r_lst = [(word, ratio(word, sample))
                                 for word in suspended]
                        word, r = max(r_lst, key=lambda x: x[1])
                        if (r > 0.825) or ('-' in word and r > 0.6):
                            counter[word] += 1
                            print(word.upper(), row[0], row[1], file=out)
    return counter


def getter_substitutes(log_lst, txt_corpus, output_path, context):
    subst_dict = defaultdict(lambda: defaultdict(list))

    def aplicate(row):
        lst = row.split()
        return (' '.join(lst[:-2]), int(lst[-2]), int(lst[-1]))

    for file in log_lst:
        with open(file, 'r') as pos_file:
            for row in pos_file:
                word, row_id, shift = aplicate(row)
                subst_dict[word][row_id].append(shift)

    def enum_getter(dct):
        result = []
        for word, rows_dict in dct.items():
            for row_id in rows_dict:
                result.append((row_id, word))
        result.sort(key=lambda x: x[0])
        for j, k in result:
            yield j, k
        yield 0, 0

    def get_token_positions(line, sentences_pos):
        result = []
        tokens = []
        last = 0
        acc = 0
        for pos in sentences_pos:
            delta = pos-acc
            tk = [t for t in line[:delta].split() if t]
            last += len(tk)
            tokens.extend(tk)
            line = line[delta:]
            acc = pos
            result.append(last)
        tk = [t for t in line.split() if t]
        tokens.extend(tk)
        return result, tokens

    gen = enum_getter(subst_dict)
    j, k = next(gen)

    with open(txt_corpus, 'r') as corpus:
        for i, line in enumerate(corpus):
            while i == j:
                mark = 0
                sentences = subst_dict.get(k, None)
                if sentences is None:
                    continue
                sentences_pos = sentences[j]
                result, tokens = get_token_positions(line, sentences_pos)
                right_limit = len(tokens)
                for pos in result:
                    left = max(0, pos - context)
                    right = min(right_limit, pos + context + 1)
                    result, center = tokens[left:right], pos - left
                    with open(f'{output_path}{k}_sub.txt', 'a') as fp:
                        print(k, j*1024*16 + pos, center,
                              ' '.join(result), file=fp, sep='\t')
                j, k = next(gen)


if __name__ == '__main__':
    main(['news'])
