from itertools import cycle
import pandas as pd
import networkx as nx
from networkx.readwrite.gpickle import write_gpickle, read_gpickle
from choice import choice
from json import dump, load
from collections import defaultdict


def load_train_data(*,
                    nouns_path='../../data/training_data/training_nouns.tsv',
                    verbs_path='../../data/training_data/training_verbs.tsv'
                    ) -> pd.DataFrame:
    nouns = pd.read_csv(nouns_path, sep='\t', index_col=0)
    nouns['PARENTS'] = nouns['PARENTS'].apply(eval)
    nouns['PARENT_TEXTS'] = nouns['PARENT_TEXTS'].apply(eval)
    verbs = pd.read_csv(verbs_path, sep='\t', index_col=0)
    verbs['PARENTS'] = verbs['PARENTS'].apply(eval)
    verbs['PARENT_TEXTS'] = verbs['PARENT_TEXTS'].apply(eval)

    def separate(x):
        return list(map(str.strip, x.split(',')))
    merged = pd.concat([nouns, verbs])
    merged['TEXT'] = merged['TEXT'].apply(separate)
    print(f'Merged DataFrame of nouns and verbs:')
    merged.info()
    return merged


def get_hyperonyms_df(merged: pd.DataFrame) -> pd.DataFrame:
    def separate(x):
        return list(map(str.strip, x.split(',')))
    data = []
    for _, row in merged.iterrows():
        data.extend(list(zip(row['PARENTS'], row['PARENT_TEXTS'])))
    data = list(set(data))
    print(f'Parents zip (id, TEXT_LINE): {data[:4]}')
    hyper = pd.DataFrame(data, columns=['SYNSET_ID', 'TEXT'])
    hyper['PARENTS'] = hyper['TEXT'].apply(lambda x: [])
    hyper['TEXT'] = hyper['TEXT'].apply(separate)
    hyper.to_csv('temp.tsv', sep='\t', index=False)
    hyper = pd.read_csv('temp.tsv', sep='\t', index_col=0)
    hyper['PARENTS'] = hyper['PARENTS'].apply(eval)
    hyper['TEXT'] = hyper['TEXT'].apply(eval)
    print(f'Hyperonyms dataframe:')
    hyper.info()
    return hyper


def concat(merged, hyper) -> pd.DataFrame:
    def test_speed(data):
        from timeit import timeit
        iterations = 10000
        def f(): return data.loc['8955-N']
        t = timeit(f, number=iterations)
        print(f'Request time approximated, {t/iterations}')
    data = pd.concat([merged.drop(columns=['PARENT_TEXTS']), hyper])
    data.to_csv('temp.tsv', sep='\t')
    data = pd.read_csv('temp.tsv', sep='\t', index_col=0)
    data['PARENTS'] = data['PARENTS'].apply(eval)
    data['TEXT'] = data['TEXT'].apply(eval).apply(set)
    print(f'Concatenated dataframe of hyperonyms and hyponyms: ')
    data.info()
    test_speed(data)
    return data


def create_translate_tables(data: pd.DataFrame, prefix, *,
                            txt2id_name=None,
                            id2txt_name=None
                            ):
    if txt2id_name is None:
        txt2id_name = f'../../data/translate_tables/{prefix}_txt2id.json'
    if id2txt_name is None:
        id2txt_name = f'../../data/translate_tables/{prefix}_id2txt.json'
    translate_table_txt2id = defaultdict(set)
    translate_table_id2txt = defaultdict(set)
    for synset, row in data.iterrows():
        translate_table_id2txt[synset].update(row['TEXT'])
    for synset, txt_set in translate_table_id2txt.items():
        for txt in txt_set:
            translate_table_txt2id[txt].add(synset)

    def normalize(dict_of_set):
        return {k: list(dict_of_set[k]) for k in dict_of_set}
    table_txt2id = normalize(translate_table_txt2id)
    table_id2txt = normalize(translate_table_id2txt)
    print(len(table_txt2id), len(table_id2txt))
    print(f'Saving translate table from text to synset_id into {txt2id_name}')
    with open(txt2id_name, 'w') as file:
        dump(table_txt2id, file)
    print(f'Saving translate table from synset_id to text into {id2txt_name}')
    with open(id2txt_name, 'w') as file:
        dump(table_id2txt, file)
    return table_txt2id, table_id2txt


def create_id_graph(data: pd.DataFrame, *,
                    file_name='../../data/training_data/data_graph.bin'
                    ) -> nx.DiGraph:
    synsets = nx.DiGraph()
    print(f'Creating NetworkX directed graph of synsets_ids')
    for idt, row in data.iterrows():
        hypo = idt
        for hyper in row['PARENTS']:
            synsets.add_edge(hypo, hyper, weight=1.0)
    print(f'Saving graph of synsets_ids (pickle) to {file_name}')
    write_gpickle(synsets, file_name)
    return synsets


def create_word_graph(data: pd.DataFrame, *,
                      file_name='../../data/training_data/word_graph.bin'
                      ) -> nx.DiGraph:
    texts = nx.DiGraph()
    print(f'Creating NetworkX directed graph of synset texts...')
    length = len(data.index)
    counter = length
    for idt, row in data.iterrows():
        hypo = idt
        attrs = {idt: row['TEXT']}
        txt = row['TEXT']
        for i in txt:
            texts.add_node(i, **attrs)
            for hyper in row['PARENTS']:
                parent_txts = data.loc[hyper]['TEXT']
                parent_attrs = {hyper: parent_txts}
                for j in parent_txts:
                    texts.add_node(j, **parent_attrs)
                    texts.add_edge(i, j, **attrs)
        if counter % 1000 == length % 1000:
            print(f'{counter} from {length}')
        counter -= 1
    print(f'Saving it to {file_name}')
    write_gpickle(texts, file_name)
    print(nx.info(texts))
    return texts


def test_graph(synsets) -> bool:
    test_id = '111485-N'
    hypers = set(synsets.successors(test_id))
    hypos = set(synsets.predecessors(test_id))
    print(f'Hyperonyms of {test_id} are {hypers}.')
    print(f'Hyponyms of {test_id} are {hypos}.')
    return not(hypers) and (len(hypos) == 1)


def stat(texts: nx.DiGraph):
    components = list(nx.weakly_connected_components(texts))
    components.sort(key=lambda x: len(x))
    print(f'Число компонент связности графа: {len(components)}')
    limit = len(components) - 1
    cluster = set.union(*[s for s in components[0:limit]])
    print(f'Размер кластеров, не имеющих пересечений с основными \
по парам гиперонимов или гипонимов: \
{len(cluster)} синсетов всего.')
    return components


def split_train_dev(word_graph, table_txt2id, *,
                    dev_part=1/3,
                    dev_name='../../data/valid_data/valid_graph.bin',
                    train_name='../../data/training_data/train_graph.bin'):
    def func(node):
        if len(node.split()) == 1:
            is_hyponym_1lvl = True
            for synset in table_txt2id[node]:
                if list(word_graph.predecessors(synset)):
                    is_hyponym_1lvl = False
            return is_hyponym_1lvl
        else:
            return False

    def split_graph(graph, dev_words):
        dev_graph = nx.DiGraph()
        hyponyms = [synset for t in dev_words for synset in table_txt2id[t]]
        dev_nodes = hyponyms.copy()
        for t in hyponyms:
            dev_nodes.extend(list(graph.successors(t)))
        dev_graph = graph.subgraph(dev_nodes).copy()
        train_graph = graph.copy()
        train_graph.remove_nodes_from(hyponyms)
        isolated = list(nx.isolates(train_graph))
        train_graph.remove_nodes_from(isolated)
        return train_graph, dev_graph, hyponyms, dev_nodes

    def test_speed(texts):
        from timeit import timeit
        def f(): return texts['111485-N']
        iterations = 10000
        t = timeit(f, number=iterations)
        print(f'Request time approximated, {t/iterations}')

    hyponyms = list(filter(func, table_txt2id))
    size = round(len(hyponyms) * dev_part)
    dev_words = choice(hyponyms, size)
    print(f'Summary, {len(dev_words)} hyponyms (single word)')

    train_graph, dev_graph, dev_hyponyms, dev_synsets = split_graph(
        word_graph, dev_words)

    print(f'Saving Dev (!) graph of words and to {dev_name}')
    print(f'Number of nodes in Dev Graph: {len(dev_graph)}')
    print(f'Saving Train (!) graph of words to {train_name}')
    write_gpickle(train_graph, train_name)
    print(f'Number of nodes in Train Graph: {len(train_graph)}')
    test_speed(word_graph)
    return train_graph, dev_graph, dev_words, dev_synsets


def create_QA(graph, words, table_txt2id, question_file, answer_file):
    from itertools import cycle
    valid_data = []
    for w in words:
        hyperonyms = []
        for synset in table_txt2id[w]:
            hyperonyms.extend(list(graph.successors(synset)))
        right_answer = hyperonyms
        for synset in hyperonyms.copy():
            right_answer.extend(list(graph.successors(synset)))
        tmp = list(zip(
            cycle([w]),
            right_answer))
        valid_data.extend(tmp)
    valid_df = pd.DataFrame(valid_data, columns=['QUESTION', 'ANSWER'])
    valid_df.drop(columns='ANSWER').drop_duplicates()


def load_full_wordnet(path='../../data/training_data/RuWordNetDF.tsv'):
    data = load_train_data(nouns_path = path)
    table_txt2id, _ = create_translate_tables(data, 'rwns')
    synset_graph = create_id_graph(data)
    if test_graph(synset_graph):
        print('Graph tested correctly')
    components = stat(synset_graph)


def load_train_valid(
    path_ask='../../data/dev_ask.tsv',
    path_true='../../data/truth.tsv'
):
    merged = load_train_data()
    hyper = get_hyperonyms_df(merged)
    data = concat(merged, hyper)
    table_txt2id, _ = create_translate_tables(data, 'data')
    synset_graph = create_id_graph(data)
    if test_graph(synset_graph):
        print('Graph tested correctly')
    components = stat(synset_graph)
    train, dev, dev_words, dev_synsets = split_train_dev(
        synset_graph, table_txt2id)
    create_translate_tables(data.drop(dev_synsets), 'train')
    dev_table_txt2id, _ = create_translate_tables(data.loc[dev], 'dev')
    create_QA(dev, dev_words, dev_table_txt2id, path_ask, path_true)


if __name__ == '__main__':
    load_train_valid()
    load_full_wordnet()
