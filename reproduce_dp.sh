#!/usr/bin/env sh
rm -f nouns.zip
rm -f verbs.zip
pushd src
pushd data_utils
python3 graph_creation.py
popd
python3 engineer.py --mean_file './additional_resources/meaning.tsv' --metric_limit 30 --data_set 'data' fire
popd
zip nouns.zip nouns.tsv
zip verbs.zip verbs.tsv
