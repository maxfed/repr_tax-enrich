# dir_words - related path to the directory with words
#  file with nouns started with 'noun', with verbs - 'verb'
dir_words=data/main-and-test
topk=300
dist=simcosmul  # simcosmul or sim
is_posmap=True  # POS mapping
levels=(1,2)  # Hypernyms ordering and merging.

# Compute Word2Vec nearest neigbours
cd ./src/w2v
bash select_w2v_topk.sh ../../$dir_words/nouns_private.tsv $topk
bash select_w2v_topk.sh ../../$dir_words/verbs_private.tsv $topk
#cd ../..

# Prediction
python reproduce_w2v.py $dir_words/nouns_private.tsv $topk $levels $is_posmap $dist # MAP = 0.428, MRR = 0.459
python reproduce_w2v.py $dir_words/verbs_private.tsv $topk \(1,\) False sim # MAP = 0.373, MRR = 0.420
