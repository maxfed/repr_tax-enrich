import sys
from collections import OrderedDict, Counter, defaultdict
sys.path.append('./src/')
from ruwordnet.ruwordnet_reader import RuWordnet
from collections import defaultdict
import pandas as pd
from pymorphy2 import MorphAnalyzer
import os
import numpy as np
from itertools import product
from pathlib import Path
from zipfile import ZipFile
wvdf = None

# LOAD RuWordNet
wn = RuWordnet('./data/ruwordnet.db', '../../taxonomy-enrichment/data/')
print('RuWordnet loaded: ', {m: len(getattr(wn, m)()) for m in dir(wn) if m.startswith('get_all')})
wn_sense2sids_df = pd.DataFrame.from_records(wn.get_all_senses()).groupby(2).agg({1: set})
wn_sense2sids = defaultdict(lambda: {})
wn_sense2sids.update(wn_sense2sids_df.to_dict()[1])
wn_sid2senses = pd.DataFrame.from_records(wn.get_all_senses()).groupby(1).agg({2: set})[2].to_dict()
sdf = pd.DataFrame.from_records(wn.get_all_senses(), columns=['sense_id', 'synset_id', 'sense_str'])
sdf.sense_str = sdf.sense_str.str.lower()
sdf['synset_str'] = sdf.synset_id.apply(wn.get_name_by_id).str.lower()

def get_hypers(synset_ids):
  return [h for sid in synset_ids for h in wn.get_hypernyms_by_id(sid)]

def add_labels(df):
  df['hypers'] = df.w.apply(lambda w: {hid for sid in wn_sense2sids[w] for hid in wn.get_hypernyms_by_id(sid)})
  df['hypers2'] = df.hypers.apply(lambda synsets: synsets.union(get_hypers(synsets)))
  df['hypers3'] = df.hypers2.apply(lambda synsets: synsets.union(get_hypers(synsets)))
  df['hypers_str'] = df.hypers.apply(lambda sids: '/'.join(wn.get_name_by_id(sid) for sid in sids))
  df['hypers2_str'] = df.hypers2.apply(lambda sids: '/'.join(wn.get_name_by_id(sid) for sid in sids))

def sids2str(v):
  return '/'.join(wn.get_name_by_id(sid) for sid in v)

def create_df(path_words):
  df = pd.read_csv(path_words, header=None)
  df = df.rename(columns={0: 'w'})
  part = os.path.basename(path_words)
  pos = 'n' if part.startswith('noun') or part.endswith('_n') else ('v' if part.startswith('verb') or part.endswith('_v') else None)
  assert pos is not None, f'expected file with words "{part}" starts with "noun" or "verb"'
  df['pos'] = pos
  add_labels(df)
  print(path_words, len(df), pos)
  return df

# Loading word2vec dataframe
def load_wvdf(part, base_dir, dist='simcosmul', model='all.norm-sz500-w10-cb0-it3-min5.w2v',
              topk=None, minscore=None):
  res_first = list(Path(base_dir).glob(f'{part}*{model}.{dist}'))[0]
  print('Loading w2v nns: ', res_first)
  wvdf = pd.read_csv(res_first, sep='\t')
  print(
    f'WARNING: {part}: no nearest neighbours for {wvdf.sims.isnull().sum()} ({wvdf.sims.isnull().mean() * 100}%) words!')
  wvdf = wvdf.dropna()
  # wvdf.sims = wvdf.sims.str.replace('[^(\w\- )]','')
  wvdf.sims = wvdf.sims.str.strip().str.split(' ')
  if topk is not None:
    wvdf.sims = wvdf.sims.str[:topk]
  wvdf.scores = wvdf.scores.str.split().apply(lambda l: np.array(l).astype(np.float))
  if minscore is not None:
    wvdf.sims = wvdf.apply(lambda r: r.sims[:(r.scores >= minscore).sum()], axis=1)
  print('Number of nns: ', wvdf.sims.apply(len).describe())
  return wvdf

# Compute hypernyms of nearest neighbours
def words2sids_contains(nns):
  nn_sids = {nn: sdf[sdf.sense_str.str.contains(nn, regex=False)] for nn in nns}
  return {k: set(v.synset_id.tolist()) for k, v in nn_sids.items() if len(v)}

def words2sids_exact(nns):
  nn_sids = OrderedDict((nn, wn_sense2sids[nn.upper()]) for nn in nns)  # for each word get all synsets
  return OrderedDict((k, v) for k, v in nn_sids.items() if v)  # filter words not present in tax

class OrderedCounter(Counter, OrderedDict):
  'Counter that remembers the order elements are first encountered'
  def __repr__(self):
    return '%s(%r)' % (self.__class__.__name__, OrderedDict(self))
  def __reduce__(self):
    return self.__class__, (OrderedDict(self),)

def sids2bag(sids, debug=False):
  sids_cnt = OrderedCounter(sids)
  if debug:
    print(len(set(sids)), '/', len(sids), 'are uniq', ' '.join(f'{k}-{wn.get_name_by_id(k)}:{c}' for k, c in sids_cnt.most_common()))
  return sids_cnt

# POS mapping
def posmap(sid, pos):
  n, p = sid.split('-')
  res = sid if p.lower() == pos.lower() else '-'.join((n, pos.upper()))
  return res if wn.get_name_by_id(res) != '' else ''  # else sid

def nns2sids_posmap(nn2sids, pos):
  nn_list = [posmap(sid, pos) for v in nn2sids.values() for sid in v]
  nn_sids = sids2bag([x for x in nn_list if x != ''])
  return nn_sids

def nns2hypers_posmap(nns, pos, posmap_nns=False, posmap_hypers=False, posmap_hypers2=False, debug=False):
  # Convert substs (space separated string) to synset ids
  nn2sids = words2sids_exact(nns)
  if debug: print('/'.join(f'{nn}{get_hypers(nn2sids.get(nn, []))}' for nn in nns))

  # Count synset ids
  nn_list = [posmap(sid, pos) if posmap_nns else sid for v in nn2sids.values() for sid in v]
  nn_sids = sids2bag([x for x in nn_list if x != ''], debug=debug)

  hyper_list = [posmap(sid, pos) if posmap_hypers else sid for sid in get_hypers(nn_sids)]
  hyper_sids = sids2bag([x for x in hyper_list if x != ''], debug=debug)

  hyper2_list = [posmap(sid, pos) if posmap_hypers2 else sid for sid in get_hypers(hyper_sids)]
  hyper2_sids = sids2bag([x for x in hyper2_list if x != ''], debug=debug)
  return nn_sids, hyper_sids, hyper2_sids

def word2hypers_posmap1(w, pos, a=True, b=True, c=True, hlevels=(1, 2)):
  w = w.lower()
  if not (wvdf.word == w).sum():
    return []
  nns = wvdf[wvdf.word == w].sims.iloc[0]
  # nns = [sim for sim in sims if wn_sense2sids[w.upper()] != wn_sense2sids[sim.upper()]] # если на dev, если на private, то убрать
  level2sids = nns2hypers_posmap(nns, pos, posmap_nns=a, posmap_hypers=b, posmap_hypers2=c)
  res_hypers = level2sids[hlevels[0]]
  for level in hlevels[1:]:
    for word in level2sids[level]:
      dic_w = res_hypers.get(word, 0)
      dic_w += level2sids[level][word]
      res_hypers[word] = dic_w
  return [k for k, c in res_hypers.most_common(10)]

ma = MorphAnalyzer()
ma_cache = {}
def parse(w):
  if w not in ma_cache:
    ma_cache[w] = ma.parse(w)
  return ma_cache[w]

def parse_mwe(nn=' участников акции', debug=False):
  words = nn.strip().split()
  q = [{h.normal_form for h in parse(w)}.union({w}) for w in words]
  q = [{nf for nf in nfs}.union({nf.replace('ё', 'е') for nf in nfs if 'ё' in nf}) for nfs in q]
  from itertools import product
  forms = [' '.join(ww) for ww in product(*q)]
  if debug: print(forms)
  return forms

def words2sids_lemm(nns):
  nn_sids = OrderedDict((nn, set.union(*(set(wn_sense2sids[form.upper()]) for form in parse_mwe(nn, debug=False)))) for nn in nns)
  return OrderedDict((k, v) for k, v in nn_sids.items() if v)  # filter words not present in tax

def save_preds(df, part, path_postfix='', dir_path=''):
    if dir_path != '':
        if not os.path.isdir(dir_path):
            os.mkdir(dir_path)
        if dir_path[-1] != '/':
            dir_path += '/'
    fname = f'{dir_path}{part}{path_postfix}.pred.tsv'
    pred_fpath = Path(fname)
    with open(fname,'w') as outp:
        outp.write('\n'.join(f'{r.w}\t{pred}'  for i,r in df.iterrows() for pred in r.pred))
    print('Saved preds to: ', fname)
    with ZipFile(fname+'.zip', 'w') as myzip:
        myzip.write(pred_fpath, pred_fpath.name)

def reproduce_w2v(path_words, topk=300, levels=(1,2,0), is_posmap=True, dist='simcosmul'):
  df = create_df(path_words)
  # Load word2vec nearest neighbours
  part = os.path.basename(path_words)
  global wvdf
  wvdf = load_wvdf(part, './w2v/res', dist, 'all.norm-sz500-w10-cb0-it3-min5.w2v', topk=topk)
  # Compute hypernyms for target word
  df['pred'] = df.apply(lambda r: word2hypers_posmap1(r.w, r.pos, a=is_posmap, b=is_posmap, c=is_posmap, hlevels=levels), axis=1)
  save_preds(df, part)

from fire import Fire
Fire(reproduce_w2v)