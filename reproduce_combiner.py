import pandas as pd
from collections import defaultdict

input_file_dp = "nouns.zip"
input_file_hch = "nouns.zip"
output_file = "result.tsv"

if __name__ == '__main__':
    dp_result = pd.read_csv(input_file_dp, compression='zip', names=[
                            'word', 'hyper'], delimiter='\t', index_col=False)
    hch_result = pd.read_csv(input_file_hch, compression='zip', names=[
                             'word', 'hyper'], delimiter='\t', index_col=False)
    dct_dp = defaultdict(list)
    dct_hch = defaultdict(list)
    result = dict()

    def reorder(lst_prior, lst_second):
        header = [i for i in lst_prior if i in lst_second]
        body = [i for i in lst_prior if i not in lst_second]
        tail = [i for i in lst_second if i not in lst_prior]
        return header + body + tail
    for index, row in hch_result.iterrows():
        dct_hch[row['word']].append(row['hyper'])
    for index, row in dp_result.iterrows():
        dct_dp[row['word']].append(row['hyper'])
    with open(output_file, 'w', encoding='utf-8') as output_file:
        for word, lst in dct_hch.items():
            for item in reorder(lst, dct_dp['word']):
                print(word, item, sep='\t', file=output_file)
