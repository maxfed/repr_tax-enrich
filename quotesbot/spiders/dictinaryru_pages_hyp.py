# -*- coding: utf-8 -*-
import scrapy


class Dictinary_Hyp(scrapy.Spider):
    name = "Dictinary_Hyp"
    def __init__(self):
        import pandas as pd
        from re import search
        df = pd.read_csv('quotes2.csv')
        text_data = df['link'].tolist()
        flag = pd.Series([not(bool(search('first/', str(x)))) for x in text_data])
        df = df.assign(flag = flag.values)
        tf = df[df.flag].drop_duplicates()
        self.start_urls = tf['link'].tolist()
        
    def parse(self, response):
            yield {
                'word': response.css("li.active::text").extract_first(),
                'synonym': response.css('ul[class*="perechislenie synonim"]').css("li.summary::text").extract(),
                'hyperonym': response.css('ul[class*="perechislenie giperonim"]').css("li.summary::text").extract(),
                'hyponym': response.css('ul[class*="perechislenie giponim"]').css("li.summary::text").extract()
            }

