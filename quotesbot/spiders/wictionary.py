# -*- coding: utf-8 -*-
import scrapy
import re


class Wictionary(scrapy.Spider):
    name = "Wictionary"
    start_urls = [
        'https://ru.wiktionary.org/w/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%92%D1%81%D0%B5_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D1%8B&from=%D0%90'
    ]

    def parse(self, response):
        for quote in response.css("div.mw-allpages-body"):
            tmp = zip(quote.css("li > a::attr(title)").extract(),
                      quote.css("li > a::attr(href)").extract())
            tmp = list(filter(lambda x: not(re.search('[^а-яА-ЯёЁ\(,\), \-]+',x[0])), tmp))
            tmp = list(zip(*tmp))
            if (tmp):
                yield {
                    'word': list(tmp[0]),
                    'link': list(tmp[1])
                }

        next_page_url = response.css("div.mw-allpages-nav > a::attr(href)").extract()[-1]
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

