# -*- coding: utf-8 -*-
import scrapy


class Dictinary(scrapy.Spider):
    name = "Dictinary"
    start_urls = [
        f'https://dictinary.ru/ru_first/{c}.html?page={i}' for c in 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя' for i in range(300)
    ]

    def parse(self, response):
        for quote in response.css("ul.pagination"):
            yield {
                'word': quote.css("li > a::text").extract(),
                'link': quote.css("li > a::attr(href)").extract()
            }

        next_page_url = None
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))

