# -*- coding: utf-8 -*-
import scrapy
import re
import time

pattern = {
            'hyper':'<h4><span id=".D0.93.D0.B8.D0.BF.D0.B5.D1.80.D0.BE.D0.BD.D0.B8.D0.BC.D1.8B"></span><span class="mw-headline" id="Гиперонимы">Гиперонимы</span>',
            'hypo':'<h4><span id=".D0.93.D0.B8.D0.BF.D0.BE.D0.BD.D0.B8.D0.BC.D1.8B"></span><span class="mw-headline" id="Гипонимы">Гипонимы</span>',
            'syn':'<h4><span id=".D0.A1.D0.B8.D0.BD.D0.BE.D0.BD.D0.B8.D0.BC.D1.8B"></span><span class="mw-headline" id="Синонимы">Синонимы</span>',
            'mean':'<h4><span id=".D0.97.D0.BD.D0.B0.D1.87.D0.B5.D0.BD.D0.B8.D0.B5"></span><span class="mw-headline" id="Значение">Значение</span>'
        }

class Wictionary(scrapy.Spider):
    name = "Wictionary_Hyp"
    def __init__(self):
        import pandas as pd
        from re import search
        df = pd.read_csv('links.csv')
        text_data = df['link'].tolist()
        flag = pd.Series([not(bool(search('first/', str(x)))) for x in text_data])
        df = df.assign(flag = flag.values)
        tf = df[df.flag].drop_duplicates()
        start_urls = list(map(lambda x: 'https://ru.wiktionary.org' + x,
                              [i for j in tf['link'].tolist() for i in j.split(',')]))
        self.start_urls = start_urls

    def parse(self, response):
            def give_me_patt_lst(key, select, pt = r">[\w\s]+</a>"):
                string = str(select)
                flag = re.search(pattern[key], string)
                if flag is None:
                    return ''
                sec = string[flag.end():]
                sec = sec[:re.search('</ol>', sec).end()]
                return [m.group(0)[1:-4] for m in re.finditer(pt, sec)][1:]
            temp = response.css("div.mw-parser-output").extract_first()
            if hash(temp)%25 == 0:
                time.sleep(1)
            yield {
                'word': response.css("h1.firstHeading::text").extract_first(),
                'meaning': give_me_patt_lst('mean', temp, r">[а-яА-ЯёЁ\(,\),\- ]{5,}<..."),
                'synonym': give_me_patt_lst('syn', temp),
                'hyperonym': give_me_patt_lst('hyper', temp),
                'hyponym': give_me_patt_lst('hypo', temp),
            }

