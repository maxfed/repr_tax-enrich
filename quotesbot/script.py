import pandas as pd
from numpy import isnan
import re

number_of_iterations = 30
RWN = pd.read_csv('RuWordNetDF.tsv', index_col=0, sep='\t')
texts = RWN['TEXT'].to_list()


def normalize(x):
    lst = re.split(',|\(|\)|\n', x)
    lst = [x.strip() for x in lst]
    lst = [x for x in lst if x]
    return lst


taxonomy_have = set([i for j in map(normalize, texts) for i in j])
taxonomy_have
wf = pd.read_csv('wiktionary.csv', index_col=0).drop_duplicates()
hyper = wf.loc[pd.notna(wf['hyperonym'])].drop_duplicates()
hyper['hyperonym'].to_csv('hyper.tsv', header=False, sep='\t')
hypo = wf.loc[pd.notna(wf['hyponym'])].drop_duplicates()
hypo['hyponym'].to_csv('hypo.tsv', header=False, sep='\t')
syn = wf.loc[pd.notna(wf['synonym'])].drop_duplicates()
syn['synonym'].to_csv('syn.tsv', header=False, sep='\t')
etc = wf.drop(wf.loc[pd.notna(wf['hyperonym'])].index)
etc = etc.drop(etc.loc[pd.notna(etc['hyponym'])].index)
etc = etc.drop(etc.loc[pd.notna(etc['synonym'])].index)
etc = etc.drop(etc.loc[pd.isna(etc['meaning'])].index)
etc = etc.drop_duplicates()
etc.to_csv('etc.tsv', sep='\t')
del hyper, hypo, syn, etc
meaning = wf.drop(columns=['hyperonym', 'hyponym', 'synonym']).dropna()
meaning['meaning'] = meaning['meaning'].apply(lambda x: x.upper())
meaning['meaning'] = meaning['meaning'].apply(lambda x: x.split(','))
meaning['meaning'] = meaning['meaning'].apply(lambda x: [i.strip() for i in x])
meaning['meaning'] = meaning['meaning'].apply(lambda x: x[:8])
meaning['meaning'] = meaning['meaning'].apply(
    lambda x: [i for i in x if i != 'РЕКОМЕНДАЦИИ'])
meaning['meaning'] = meaning['meaning'].apply(
    lambda x: [i for i in x if i != 'НАЦИОНАЛЬНОГО КОРПУСА РУССКОГО ЯЗЫКА'])
meaning['meaning'] = meaning['meaning'].apply(
    lambda x: [i for i in x if i != 'СПИСОК ЛИТЕРАТУРЫ'])
meaning['meaning'] = meaning['meaning'].apply(
    lambda x: [i for i in x if i != '(ЦИТАТА ИЗ'])
meaning['meaning'] = meaning['meaning'].apply(
    lambda x: [i for j in x for i in j.split()])
meaning['PARENT'] = meaning['meaning'].apply(lambda x: [])
meaning.to_csv('meaning.tsv', sep='\t')


def normalize_name(x):
    if isinstance(x, float):
        return 'nan'
    if x.startswith('Викисловарь:Инкубатор/'):
        return x[len('Викисловарь:Инкубатор/'):].upper()
    else:
        return x.upper()


def get_phrases(s, lim):
    l = len(s)
    n = l-1-lim
    if n < 0:
        return s
    else:
        return [' '.join(s[i:i+n+1]) for i in range(0, l-n)]


def iterate(meaning, lim=2):
    for idt, row in meaning.iterrows():
        if not(hash(idt) % 10000):
            print(idt)
        if not(row['PARENT']):
            result = get_phrases(row['meaning'], lim)
            result = [t for t in result if t in taxonomy_have]
            if result:
                row['PARENT'].extend(result)
                taxonomy_have.add(normalize_name(idt).upper())
    meaning['len'] = meaning['PARENT'].apply(len)

for i in range(number_of_iterations):
    iterate(meaning, i)
    meaning.loc[meaning['len'] > 0].to_csv(f'meaning{i}.tsv', sep='\t')
    meaning = meaning.drop(meaning.loc[meaning['len'] > 0].index)
