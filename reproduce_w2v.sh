# path_words - related path to words
#  file with nouns started with 'noun', with verbs - 'verb'
path_words=data/main-and-test/nouns_private.tsv
topk=300
dist=simcosmul  # simcosmul or sim
is_posmap=True  # POS mapping
levels=(1,2,0)  # Hypernyms ordering and merging.

# Compute Word2Vec nearest neigbours
cd ./src/w2v
bash select_w2v_topk.sh ../../$path_words $topk
cd ../..

# Prediction
python reproduce_w2v.py $path_words $topk $levels $is_posmap $dist
